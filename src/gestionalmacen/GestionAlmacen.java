/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.user.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;

/**
 * Entry class to the project
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class GestionAlmacen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        
        if (!testDB()) {
            Helpers.showMessageFor("No se ha podido crear la BBDD. Saliendo", 0);
            System.exit(2);
        }
        
        try {
            LogManager.getLogManager().readConfiguration(new FileInputStream("src/gestionalmacen/log.properties"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        int currentSelection;
        StringBuilder mainMenu = new StringBuilder();

        // Menu Principal
        mainMenu.append("\n");
        mainMenu.append("=======================================\n");
        mainMenu.append("|   BIENVENIDO, ¿QUÉ DESEA HACER?     |\n");
        mainMenu.append("=======================================\n");
        mainMenu.append("| Opciones:                           |\n");
        mainMenu.append("|        1. Acceder a la aplicación   |\n");
        mainMenu.append("|        2. Registrar usuario         |\n");
        mainMenu.append("|        3. Salir de la aplicación    |\n");
        mainMenu.append("=======================================\n");
        mainMenu.append("\n");
        mainMenu.append("Seleccione una opción: ");
        
        Helpers.clearScreen();
        
        do {
            System.out.print(mainMenu.toString());
            
            Scanner scan = new Scanner(System.in);
            try {
                currentSelection = scan.nextInt();
            }
            catch (java.util.InputMismatchException ex){
                Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
                currentSelection = 0;
            }

            switch (currentSelection) {
            case 1:
                UserLogin.load();
                break;
            case 2:
                CreateUser.load();
                break;
            case 3:
                Helpers.showMessageFor("Gracias por utilizar el Sistema de gestión de Almacén JP", 2000);
                System.exit(0);
                break;
            default:
                Helpers.clearScreen();
                System.out.println("Selección no válida\n");
                break;

            }
        } while (true);
    }
    
    private static Boolean testDB () {
        
        Connection conn;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
                
            conn = DriverManager.getConnection(Session.DB_URL);
            
            if (checkIfTableExist(conn, "USERS") && 
                    checkIfTableExist(conn, "FAVORITOS") && 
                    checkIfTableExist(conn, "PRODUCTS"))
                return  true;
            
            return recreateDB(conn);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    private static Boolean recreateDB(Connection conn) throws SQLException  {
        
        File initialFile = new File("_other/db_recreation.sql");
        InputStream in;
        Statement st = conn.createStatement();
        
        try {
            in = new FileInputStream(initialFile);
            Scanner s = new Scanner(in);
            s.useDelimiter("(;(\r)?\n)|(--\n)");
            
            while (s.hasNext())
            {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/"))
                {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0)
                {
                    st.execute(line);
                }
            }
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally
        {
            if (st != null) st.close();
        }
        return true;
    }
    
    private static Boolean checkIfTableExist (Connection con, String tableName) {
        try {
            DatabaseMetaData meta = con.getMetaData();
            ResultSet res = meta.getTables(null, null, tableName,
                    new String[] {"TABLE"});
            if (!res.next()) 
                return false;
        } catch (SQLException ex) {
            Logger.getLogger(GestionAlmacen.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
}
