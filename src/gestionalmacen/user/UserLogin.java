/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.user;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.MainMenu;
import gestionalmacen.*;
import gestionalmacen.objects.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User login interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class UserLogin {
    
    /**
     * Loads the class
     */
    public static void load() {
        int tries = 0;
        String eMail = "";
        String password = "";
        
        StringBuilder loginMenu = new StringBuilder();

        loginMenu.append("\n");
        loginMenu.append("=======================================\n");
        loginMenu.append("|   ACCESO A LA APLICACIÓN            |\n");
        loginMenu.append("=======================================\n");
        loginMenu.append("\n");
        
        Helpers.clearScreen();
        
        do {
            System.out.print(loginMenu.toString());
            System.out.print("Introduzca su eMail ([Intro] para volver): ");
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            
            try {
                eMail = buffer.readLine();
            } catch (IOException ex) {
                Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
                Helpers.showMessageFor("ERROR: Error en la lectura. Intente de nuevo.", 2000);
            }
            
            if (eMail.length() == 0) {
                Helpers.showMessageFor("Volviendo al menú principal...", 1500);
                return;
            }                
            
            // TODO
            System.out.print("Introduzca su contraseña: ");
            try {
                password = buffer.readLine();
            } catch (IOException ex) {
                Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
                Helpers.showMessageFor("ERROR: Error en la lectura. Intente de nuevo.", 2000);
            }
            
            Session.get().setUser(User.checkLogin(eMail, Helpers.calcuclateMD5(password)));
            if (Session.get().isLogged()) {
                MainMenu.load();
                return;
            }
            
            if ( ++tries <= 5)
            {
                Helpers.showMessageFor("*** Datos de acceso incorrectos, vuelva a intentarlo. ***", 1000);
            }
            else
            {
                Helpers.showMessageFor("*** Demasiados intentos de login incorrectos. SALIENDO... ***", 2000);
                System.exit(1);
            }
        } while (true);
    }
}


