/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.user;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.objects.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Create user interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class CreateUser {
    
    /**
     * Loags the current class
     */
    public static void load() {
        
        User user = new User();
        
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
        String newValue;

        try {
            System.out.print("Nombre: ");
            newValue = buffer.readLine();
            if (newValue.length() > 0)
                user.setName(newValue);
        } catch (IOException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Apellidos: ");
            newValue = buffer.readLine();
            if (newValue.length() > 0)
                user.setSurname(newValue);
        } catch (IOException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Email: ");
            newValue = buffer.readLine();
            if (newValue.length() > 0)
                user.setMail(newValue);
        } catch (IOException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Password: ");
            newValue = buffer.readLine();
            if (newValue.length() > 0) 
                user.setPassword(Helpers.calcuclateMD5(newValue));
        } catch (IOException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        
        if (user.create())
            Helpers.showMessageFor("Usuario añadido correctamente.", 1000);
        else
            Helpers.showMessageFor("No se ha podido añadir el usuario. Compruebe que el correo electrónico es único.", 2000);
    }
}
