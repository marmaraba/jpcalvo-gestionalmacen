/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.products.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main Menu interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class MainMenu {

    /**
     * Runs MainMenu Interface
     */
    @SuppressWarnings("SleepWhileInLoop")
    public static void load() {
        
        int currentSelection;
        StringBuilder mainMenu = new StringBuilder();

        // Menu Principal
        mainMenu.append("\n");
        mainMenu.append("=======================================\n");
        
        if (Session.get().getUser().isAdmin())
            mainMenu.append("|   MENU PRINCIPAL [ADMIN]            |\n");
        else
            mainMenu.append("|   MENU PRINCIPAL                    |\n");
        
        mainMenu.append("=======================================\n");
        mainMenu.append("| Opciones:                           |\n");
        mainMenu.append("|        1. Ver productos             |\n");
        
        if (Session.get().getUser().isAdmin())
            mainMenu.append("|        2. Gestión de productos      |\n");
        else
            mainMenu.append("|        2. Ver favoritos             |\n");
        
        mainMenu.append("|        3. Salir                     |\n");
        mainMenu.append("=======================================\n");
        mainMenu.append("\n");
        mainMenu.append("Seleccione una opción: ");
        
        Helpers.clearScreen();
        
        do {
            System.out.print(mainMenu.toString());
            
            Scanner scan = new Scanner(System.in);
            try {
                currentSelection = scan.nextInt();
            }
            catch (java.util.InputMismatchException ex){
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                currentSelection = 0;
            }

            switch (currentSelection) {
                case 1:
                    ViewProducts.load();
                    continue;
                case 2:
                    if (Session.get().getUser().isAdmin())
                        ManageProducts.load();
                    else
                        ViewFavorites.load();
                    continue;
                case 3:
                    Helpers.showMessageFor("Volviendo a la pantalla de acceso...", 2000);
                    break;
                default:
                    Helpers.showMessageFor("Selección no válida", 1500);
                    continue;
            }
            
            return;
        } while (true);
    }
}
