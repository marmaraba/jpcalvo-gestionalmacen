/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.objects;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.Session;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for managing the products
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class Product {
    private Integer id = null;
    private String name;
    private Float price;
    private Integer units;
    private String color;
    private List<User> favoritos;
    
    /**
     * Empty constructor
     */
    public Product() {}
    
    /**
     * Read all products from database
     * @return the list of products
     */
    public static List<Product> getAll () {
        
        ResultSet results ;
        List<Product> productos = new ArrayList<>();
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            Connection conn = DriverManager.getConnection(Session.DB_URL);
            Statement stmt = conn.createStatement();
            results = stmt.executeQuery("SELECT * FROM products");
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("Error al recuperar la lista de productos", 2000);
            return productos;
        }
        
        try {
            while(results.next())
            {
                Product product = new Product();
                
                product.setId(results.getInt(1));
                product.setName(results.getString(2));
                product.setPrice(results.getFloat(3));
                product.setUnits(results.getInt(4));
                product.setColor(results.getString(5));
                
                productos.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return productos;
    }
    
    /**
     * Search a product by its id
     * @param id The id of the looked for product
     * @return The product or null if can't be found or an error occurs
     */
    public static Product getById(Integer id) {
        Product product = null;
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE ID=" + id);
            
            if (results.next())
            {
                product = new Product();
                product.setId(id);
                product.setName(results.getString(2));
                product.setPrice(results.getFloat(3));
                product.setUnits(results.getInt(4));
                product.setColor(results.getString(5));
            }
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }
    
    private Boolean update () {
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "UPDATE PRODUCTS SET " +
                "NAME = '" + name + "', " +
                "PRICE = " + price + ", " +
                "UNITS = " + units + ", " +
                "COLOR = '" + color + "' " +
                "WHERE ID=" + id;
            stmt.executeUpdate(query);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
    
    /**
     * Delete the current product from database
     * @return true if the product was correctly deleted.
     */
    public Boolean destroy () {
        Connection conn;
        Statement stmt;
        
        if (id == null) {
            return false;
        }
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "DELETE FROM PRODUCTS WHERE ID=" + id;
            stmt.executeUpdate(query);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
    
    /**
     * Save the current product state in DB
     * @return True if the operation was succesful
     */
    public Boolean save () {
        Connection conn;
        Statement stmt;
        
        if (id != null) {
            return update();
        }
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "INSERT INTO PRODUCTS " + 
                    "(NAME, PRICE, UNITS, COLOR)" +
                    "VALUES " +
                    "('" + name + "', " + price + ", " + units + ", '" + color + "')" ;
            stmt.executeUpdate(query);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }

    /**
     * Get the product id
     * @return
     */
    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public Float getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(Float price) {
        this.price = price;
    }

    /**
     *
     * @return
     */
    public Integer getUnits() {
        return units;
    }

    /**
     *
     * @param units
     */
    public void setUnits(Integer units) {
        this.units = units;
    }

    /**
     *
     * @return
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }
    
    /**
     *
     * @return
     */
    public List<User> getUsuariosCollection() {
        return favoritos;
    }

    /**
     *
     * @param favoritos
     */
    public void setUsuariosCollection(List<User> favoritos) {
        this.favoritos = favoritos;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "jpcalvo.almacen.objetos.Productos[ id=" + id + " ]";
    }
    
}
