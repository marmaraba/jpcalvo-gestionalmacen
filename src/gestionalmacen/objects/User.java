/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.objects;

import gestionalmacen.Session;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class User {
    private Integer id;
    private String name;
    private String surname;
    private String mail;
    private String password;
    private List<Product> favoritos;
    private Boolean isAdmin = false;

    /**
     * Constructor
     */
    public User() {
    }

    /**
     * Other constructor
     * @param mail
     * @param password
     */
    public User(String mail, String password) {
        this.mail = mail;
        this.password = password;
    }

    /**
     * More detailed constructor
     * @param name
     * @param surname
     * @param mail
     * @param password
     */
    public User(String name, String surname, String mail, String password) {
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.password = password;
    }
    
    private static User getByEmail (String email, Boolean isAdmin) {
        User admin = getByEmail(email);
        if (admin != null && isAdmin)
            admin.setAdmin();
        return admin;
    } 
    
    private static User getByEmail (String email) {
        User user = null;
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM USERS WHERE MAIL='" + email + "'");
            
            if (results.next())
            {
                user = new User();
                user.setId(results.getInt(1));
                user.setName(results.getString(2));
                user.setSurname(results.getString(3));
                user.setMail(results.getString(4));
                user.setPassword(results.getString(5));
                
                results = stmt.executeQuery("SELECT * FROM FAVORITOS WHERE USER_ID=" + user.getId());
                List<Product> favoritos = new ArrayList<>();
                while (results.next()) {
                    Product product = Product.getById(results.getInt(2));
                    if (product != null)
                        favoritos.add(product);
                }
                
                user.setFavoritos(favoritos);
            }
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /**
     * Check if the tried login is succesfull
     * @param email the user email
     * @param password the user password in MD5
     * @return User logged or null if check fails
     */
    public static User checkLogin (String email, String password) { 
        
        User user = checkAdminLogin(email, password);
        if (user != null) 
            return user;
        
        user = getByEmail(email);
        if (user == null)
            return null;
        
        if (!user.getPassword().equals(password))
            return null;
        
        return user;
    }
    
    private static User checkAdminLogin (String eMail, String password) {
        
        BufferedReader reader;
        
        try {
            reader = new BufferedReader(new FileReader(Session.ADMIN_FILE));
            String line;
            while ((line = reader.readLine()) != null)
            {
                if (!line.contains(":"))
                    continue;
                
                String[] data = line.split(":");
                if (data[0].equals(eMail) && data[1].equals(password)){
                    if (User.getByEmail(eMail) == null)
                    {
                        User newUser = new User(eMail, password);
                        newUser.create();
                    }
                    return getByEmail(eMail, true);
                } 
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Save current user state
     * @return
     */
    public Boolean save () {
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "UPDATE USERS SET " +
                "NAME = '" + name + "', " +
                "SURNAME = '" + surname + "', " +
                "MAIL = '" + mail + "', " +
                "PASSWORD = '" + password + "' " +
                "WHERE ID=" + id;
            stmt.executeUpdate(query);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        this.updateFavorites();
        
        return true;
    }
    
    private Boolean updateFavorites() {
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "DELETE FROM FAVORITOS "
                    + "WHERE USER_ID=" + id;
            stmt.executeUpdate(query);
            
            for (Product prod : this.favoritos) {
                query = "INSERT INTO FAVORITOS "
                        + "(USER_ID, PRODUCT_ID) "
                        + "VALUES "
                        + "(" + id + ", " + prod.getId() + ")";
                stmt.executeUpdate(query);
            }
            
            stmt.close();
            conn.close();
            return true;
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Creates new user with current object state
     * @return
     */
    public Boolean create () {
        Connection conn;
        Statement stmt;
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        
            conn = DriverManager.getConnection(Session.DB_URL);
            stmt = conn.createStatement();
            String query = "INSERT INTO USERS " +
                    "(NAME, SURNAME, MAIL, PASSWORD) " +
                    "VALUES ("
                    + "'" + name + "', "
                    + "'" + surname + "', "
                    + "'" + mail + "', "
                    + "'" + password + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        this.id = getByEmail(mail).getId();
        
        return true;
    }
    
    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return
     */
    public String getMail() {
        return mail;
    }

    /**
     *
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public List<Product> getFavoritos() {
        return favoritos;
    }
    
    /**
     *
     * @param product
     */
    public void addFavorito (Product product) {
        if (this.favoritos.contains(product))
            return;
        this.favoritos.add(product);
    }
    
    /**
     *
     * @param product
     */
    public void deleteFavorito (Product product) {
        this.favoritos.remove(product);
    }

    /**
     *
     * @param favoritos
     */
    public void setFavoritos(List<Product> favoritos) {
        this.favoritos = favoritos;
    }
    
    /**
     *
     * @return
     */
    public Boolean isAdmin() {
        return isAdmin;
    }
    
    private void setAdmin() {
        isAdmin = true;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "jpcalvo.almacen.objetos.Usuarios[ id=" + id + " ]";
    }
    
}
