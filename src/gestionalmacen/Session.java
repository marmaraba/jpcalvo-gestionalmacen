/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen;

import gestionalmacen.objects.*;

/**
 *
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class Session {
    
    /**
     * Admin users location
     */
    public static final String ADMIN_FILE = "src/gestionalmacen/admin.conf";

    /**
     * Database connection string
     */
    public static final String DB_URL = "jdbc:derby://localhost:1527/sample;user=app;password=app";    
    private static Session currentSession = null;
    private User loggedUser;
    
    /**
     * Singleton for user session
     * @return the current session
     */
    public static Session get() {
        if (currentSession == null )
            currentSession = new Session();
        return currentSession;
    }
    
    /**
     * Get current logged user
     * @return the current logged user
     */
    public User getUser() {
        return loggedUser;
    }
    
    /**
     * Set current logged user
     * @param user the new logged user
     */
    public void setUser(User user) {
        this.loggedUser = user;
    }
    
    /**
     * Check if user is logged
     * @return true if user is logged
     */
    public Boolean isLogged() {
        return (loggedUser != null);
    }
}
