/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.helpers;

import gestionalmacen.objects.Product;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Functions widely used accross the application
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class Helpers {

    /**
     * Clears the console contents
     */
    public static void clearScreen() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(Helpers.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    /**
     * Shows a message in the console
     * @param message The message to be shown
     * @param milliseconds The time the message will be available
     */
    public static void showMessageFor (String message, int milliseconds) {
        Helpers.clearScreen();
        System.out.println(message);
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
            Logger.getLogger(Helpers.class.getName()).log(Level.SEVERE, null, ex);
        }
        Helpers.clearScreen();
    }
    
    /**
     * Show in screen the products received as argument
     * @param productos list of products
     */
    public static void printProductsTable(List<Product> productos) {
        System.out.println(new String(new char[80]).replace("\0", "-"));
        System.out.format("| %-30s | %-12s | %-10s | %-15s |%n", "Nombre", "Precio", "Unidades", "Color");
        System.out.println(new String(new char[80]).replace("\0", "-"));
        
        productos.forEach((product) -> {
            System.out.format("| %-30.30s | %10.2f € | %10d | %-15.15s |%n",
                    product.getName(),
                    product.getPrice(),
                    product.getUnits(), 
                    product.getColor());
        });
        
        System.out.println(new String(new char[80]).replace("\0", "-"));
    }
    
    /**
     * Calculate MD5 hash of a string
     * @param originalString the string to be hashed
     * @return the md5 hashed string
     */
    public static String calcuclateMD5 (String originalString) {
        MessageDigest md ;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Helpers.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        md.reset();
        md.update(originalString.getBytes());
        
        byte[] digest = md.digest();
        BigInteger bigInt = new BigInteger(1,digest);
        String calculatedHash = bigInt.toString(16);
        while(calculatedHash.length() < 32 ){
            calculatedHash = "0"+calculatedHash;
        }
        
        return calculatedHash;
    }
}