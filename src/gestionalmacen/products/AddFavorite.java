/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.*;
import gestionalmacen.objects.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Add favorite product interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class AddFavorite {
    
    /**
     * Load the class
     */
    public static void load() {
        
        do {
            int productId;
            List<Product> products = Product.getAll();
            StringBuilder addFavoriteMenu = new StringBuilder();
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            
            addFavoriteMenu.append("\n");
            addFavoriteMenu.append("=======================================\n");
            addFavoriteMenu.append("|   AÑADIR PRODUCTO FAVORITO          |\n");
            addFavoriteMenu.append("=======================================\n");
            addFavoriteMenu.append("| Lista de productos:                 |\n");
            for (Product prod : products) 
                addFavoriteMenu.append(String.format("|   %3d. %-27s  |\n", prod.getId(), prod.getName()));
            addFavoriteMenu.append("=======================================\n");
            addFavoriteMenu.append("\n");
            addFavoriteMenu.append("Seleccione el producto que desea añadir como favorito ([Intro] para volver): ");
        
            
            Helpers.clearScreen();
            System.out.print(addFavoriteMenu.toString());
            
            try {
                String input = buffer.readLine();
                if (input.length() == 0)
                    return;
                productId = Integer.parseInt(input);
                if (Product.getById(productId) == null) {
                    Helpers.showMessageFor("ID de producto no válido.", 1000);
                    continue;
                }
            } catch (NumberFormatException | IOException ex) {
                Logger.getLogger(AddFavorite.class.getName()).log(Level.SEVERE, null, ex);
                Helpers.showMessageFor("Entrada no válida, inténtelo de nuevo.", 1000);
                continue;
            }
            
            Session.get().getUser().addFavorito(Product.getById(productId));
            Session.get().getUser().save();
        } while (true);
    }
}
