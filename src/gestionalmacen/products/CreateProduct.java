/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.objects.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Create product interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class CreateProduct {
    
    /**
     * Run CreateProduct interface
     */
    public static void load() {
        
        Product product = new Product();
        
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("Nombre: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setName(newValue);
        } catch (IOException ex) {
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Precio: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setPrice(Float.parseFloat(newValue));
        } catch (IOException | NumberFormatException ex) {
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Unidades: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setUnits(Integer.parseInt(newValue));
        } catch (IOException | NumberFormatException ex) {
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Color: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setColor(newValue);
        } catch (IOException ex) {
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        
        if (product.save())
            Helpers.showMessageFor("Producto añadido correctamente.", 1000);
        else
            Helpers.showMessageFor("*** Error al añadir. ***", 2000);
    }
}
