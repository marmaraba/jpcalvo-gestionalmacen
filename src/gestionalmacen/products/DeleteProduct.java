/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.*;
import gestionalmacen.objects.Product;
import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Delete product interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class DeleteProduct {
    
    /**
     * Run DeleteProduct interface
     */
    public static void load() {
        
        if (!Session.get().getUser().isAdmin()) {
            Helpers.showMessageFor("*** No tienes permiso para estar aquí. Se cerrará la aplicación. ***", 2000);
            System.exit(0);
        }
        
        
        do {
            int productId;
            List<Product> products = Product.getAll();
            StringBuilder deleteProductMenu = new StringBuilder();
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            
            deleteProductMenu.append("\n");
            deleteProductMenu.append("=======================================\n");
            deleteProductMenu.append("|   ELIMINAR PRODUCTO                 |\n");
            deleteProductMenu.append("=======================================\n");
            deleteProductMenu.append("| Lista de productos:                 |\n");
            products.forEach((prod) -> { 
                deleteProductMenu.append(String.format("|   %3d. %-27s  |\n", prod.getId(), prod.getName()));
            });
            deleteProductMenu.append("=======================================\n");
            deleteProductMenu.append("\n");
            deleteProductMenu.append("Seleccione el producto que desea eliminar ([Intro] para volver): ");
        
            
            Helpers.clearScreen();
            System.out.print(deleteProductMenu.toString());
            
            try {
                String input = buffer.readLine();
                if (input.length() == 0)
                    return;
                productId = Integer.parseInt(input);
            } catch (NumberFormatException | IOException ex) {
                Logger.getLogger(DeleteProduct.class.getName()).log(Level.SEVERE, null, ex);
                Helpers.showMessageFor("Entrada no válida, inténtelo de nuevo.", 1000);
                continue;
            }
            
            Product.getById(productId).destroy();
        } while (true);
    }
}
