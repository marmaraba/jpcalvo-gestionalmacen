/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.*;
import gestionalmacen.helpers.Helpers;
import gestionalmacen.objects.Product;
import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Modify product interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class ModifyProduct {
    
    /**
     * Loads interface
     */
    public static void load() {
        
        if (!Session.get().getUser().isAdmin()) {
            Helpers.showMessageFor("*** No tienes permiso para estar aquí. Se cerrará la aplicación. ***", 2000);
            System.exit(0);
        }
        
        
        do {
            int productId;
            List<Product> products = Product.getAll();
            StringBuilder modifyProductMenu = new StringBuilder();
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            
            modifyProductMenu.append("\n");
            modifyProductMenu.append("=======================================\n");
            modifyProductMenu.append("|   MODIFICAR PRODUCTO                |\n");
            modifyProductMenu.append("=======================================\n");
            modifyProductMenu.append("| Lista de productos:                 |\n");
            products.forEach((prod) -> { 
                modifyProductMenu.append(String.format("|   %3d. %-27s  |\n", prod.getId(), prod.getName()));
            });
            modifyProductMenu.append("=======================================\n");
            modifyProductMenu.append("\n");
            modifyProductMenu.append("Seleccione el producto que desea modificar ([Intro] para volver): ");
        
            
            Helpers.clearScreen();
            System.out.print(modifyProductMenu.toString());
            
            try {
                String input = buffer.readLine();
                if (input.length() == 0)
                    return;
                productId = Integer.parseInt(input);
            } catch (NumberFormatException | IOException ex) {
                Logger.getLogger(ModifyProduct.class.getName()).log(Level.SEVERE, null, ex);
                Helpers.showMessageFor("Entrada no válida, inténtelo de nuevo.", 1000);
                continue;
            }
            
            modify(productId);
        } while (true);
    }
    
    private static void modify(Integer id) {
        
        Product product = Product.getById(id);
        if (product == null) {
            Helpers.showMessageFor("*** El producto con ID " + id + " no existe. Volviendo... ***", 2000);
            return;
        }
        
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("Nombre [" + product.getName() + "]: ");
            String newName = buffer.readLine();
            if (newName.length() > 0)
                product.setName(newName);
        } catch (IOException ex) {
            Logger.getLogger(ModifyProduct.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Precio [" + product.getPrice()+ "]: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setPrice(Float.parseFloat(newValue));
        } catch (IOException ex) {
            Logger.getLogger(ModifyProduct.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Unidades [" + product.getUnits()+ "]: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setUnits(Integer.parseInt(newValue));
        } catch (IOException ex) {
            Logger.getLogger(ModifyProduct.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        try {
            System.out.print("Color [" + product.getColor() + "]: ");
            String newValue = buffer.readLine();
            if (newValue.length() > 0)
                product.setColor(newValue);
        } catch (IOException ex) {
            Logger.getLogger(ModifyProduct.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("ERROR: Error en la lectura.", 2000);
            return;
        }
        
        if (product.save())
            Helpers.showMessageFor("Producto modificado correctamente.", 1000);
        else
            Helpers.showMessageFor("*** Error al actualizar. ***", 2000);
    }
}
