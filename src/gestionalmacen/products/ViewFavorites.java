/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.Session;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * View favorites interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class ViewFavorites {
    
    /**
     * Loads the current class
     */
    public static void load () {
        
        Helpers.clearScreen();
        System.out.println("Esta es su lista de productos favoritos: \n");

        Helpers.printProductsTable(Session.get().getUser().getFavoritos());
        
        System.out.println("Pulsa una tecla para continuar a las opciones...");
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(ViewFavorites.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        StringBuilder viewFavoriteMenu = new StringBuilder();

        viewFavoriteMenu.append("\n");
        viewFavoriteMenu.append("=======================================\n");
        viewFavoriteMenu.append("|   GESTIÓN DE PRODUCTOS              |\n");
        viewFavoriteMenu.append("=======================================\n");
        viewFavoriteMenu.append("| Opciones:                           |\n");
        viewFavoriteMenu.append("|        1. Añadir favorito           |\n");
        viewFavoriteMenu.append("|        2. Eliminar favorito         |\n");
        viewFavoriteMenu.append("|        3. Volver                    |\n");
        viewFavoriteMenu.append("=======================================\n");
        viewFavoriteMenu.append("\n");
        viewFavoriteMenu.append("Seleccione una opción: ");
        
        Helpers.clearScreen();
        
        do {
            Integer currentSelection;
            System.out.print(viewFavoriteMenu.toString());
            
            Scanner scan = new Scanner(System.in);
            try {
                currentSelection = scan.nextInt();
            }
            catch (java.util.InputMismatchException ex){
                Logger.getLogger(ViewFavorites.class.getName()).log(Level.SEVERE, null, ex);
                currentSelection = 0;
            }

            switch (currentSelection) {
                case 1:
                    AddFavorite.load();
                    continue;
                case 2:
                    DeleteFavorite.load();
                    continue;
                case 3:
                    return;
                default:
                    Helpers.showMessageFor("Selección no válida", 1500);
            }
        } while (true);
    }
}
