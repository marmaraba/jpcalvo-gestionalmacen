/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.objects.Product;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * View products interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class ViewProducts {
    
    /**
     * Loads the current class
     */
    public static void load () {
        
        Helpers.clearScreen();
        System.out.println("Esta es la lista de productos en la base de datos: \n");

        Helpers.printProductsTable(Product.getAll());
        
        System.out.println("\n\n");
        System.out.println("Pulse una tecla para salir de esta pantalla.");
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(ViewProducts.class.getName()).log(Level.SEVERE, null, ex);
            Helpers.showMessageFor("Volviendo a la pantalla anterior...", 4000);
        }
    }
    
}
