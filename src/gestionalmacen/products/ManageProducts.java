/*

 *     Copyright (C) 2018  Juan Pablo Calvo
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package gestionalmacen.products;

import gestionalmacen.helpers.Helpers;
import gestionalmacen.Session;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/** 
 * Manage product interface class
 * @author <a href="mailto:jpcalvo@jumasoluciones.es">Juan Pablo Calvo</a>
 */
public class ManageProducts {
    
    /**
     *  Run ManageProducts interface
     */
    public static void load() {
        
        int currentSelection;
        
        if (!Session.get().getUser().isAdmin()) {
            Helpers.showMessageFor("*** No tienes permiso para estar aquí. Se cerrará la aplicación. ***", 2000);
            System.exit(0);
        }
        
        StringBuilder manageProductMenu = new StringBuilder();

        manageProductMenu.append("\n");
        manageProductMenu.append("=======================================\n");
        manageProductMenu.append("|   GESTIÓN DE PRODUCTOS              |\n");
        manageProductMenu.append("=======================================\n");
        manageProductMenu.append("| Opciones:                           |\n");
        manageProductMenu.append("|        1. Modificar producto        |\n");
        manageProductMenu.append("|        2. Eliminar producto         |\n");
        manageProductMenu.append("|        3. Nuevo producto            |\n");
        manageProductMenu.append("|        4. Volver                    |\n");
        manageProductMenu.append("=======================================\n");
        manageProductMenu.append("\n");
        manageProductMenu.append("Seleccione una opción: ");
        
        Helpers.clearScreen();
        
        do {
            System.out.print(manageProductMenu.toString());
            
            Scanner scan = new Scanner(System.in);
            try {
                currentSelection = scan.nextInt();
            }
            catch (java.util.InputMismatchException ex){
                Logger.getLogger(ManageProducts.class.getName()).log(Level.SEVERE, null, ex);
                currentSelection = 0;
            }

            switch (currentSelection) {
                case 1:
                    ModifyProduct.load();
                    continue;
                case 2:
                    DeleteProduct.load();
                    continue;
                case 3:
                    CreateProduct.load();
                    continue;
                case 4:
                    Helpers.showMessageFor("Volviendo al Menú principal...", 1000);
                    break;
                default:
                    Helpers.showMessageFor("Selección no válida", 1500);
                    continue;
            }
            
            return;
        } while (true);
    }
}
